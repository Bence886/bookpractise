#include "PointersArraysStructs.h"
#include <iostream>

using namespace std;

void one()
{
	char a = 'a';
	char* ch = &a;
	int num[10];
	int * pointerNum = num;

	char** chh = &ch;

	const int ten = 10;
	const int* tenPtr = &ten;

	int onenum = 10;
	int const* cnum = &onenum;
}

void two()
{
	typedef const unsigned char CHAR;
	typedef int* intptr;
	typedef char** chptr;
	
}

void swap1(int * a, int * b)
{
	int c = *a;
	a = b;
	b = &c;
}

void swap2(int & a, int & b)
{
	int c = a;
	a = b;
	b = c;


}

void writearray(int * param, int length)
{
	for (int i = 0; i < length; i++)
	{
		cout << *(param+i);
	}
}
