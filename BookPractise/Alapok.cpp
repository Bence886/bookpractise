#include <iostream>
#include <limits>
#include <string>
#include <complex>

using namespace std;

#include "Alapok.h"

void helloWorld() 
{
	std::cout << "Hello";
	
}

struct Date
{
	int d, y, m;
};

void declarations()
{
	char ch = 'a';
	string s = "Random";
	int count = 1;
	const double pi = 3.14;
	extern int error_number;

	char* name = "Name_1";
	char* nums[] = {"Nulla", "Egy", "Ketto", "Harom"};

	typedef complex<short> Point;
	struct User;

	enum beer {A, b, c};

	Date date;
	date.d = 1;
	date.m = 2;
	date.y = 3;

	beer en = beer::A;
}

void sizes()
{
	int a = 10;
	int *b = &a;
	cout << b;
	cout << "\t";
	cout << *b;
}

void chars()
{
	char a = 'a';
	for (int i = 0; i < 26; i++)
	{
		cout << a++;
	}
}

template<class T> T abs(T a) { return a < 0 ? -a : a; }

namespace NS { int a; }

int day(Date* p)
{
	return p->d;
}