#include "Methods.h"

void One(char * p1, int & p2)
{
}

void Asd()
{
	efct a = &One;
	char ch;
	int i;
	a(&ch, i);
}

efct fvretfv()
{
	return efct();
}

Token_Value currToken;
double numVal;
string stringVal;
map<string, double> table;

double expr(bool get)
{
	double left = term(get);

	for (;;)
	{
		switch (currToken)
		{
		case PLUS: left += term(get);
			break;
		case MINUS:left -= term(get);
			break;
		default:
			return left;
		}
	}
}

double term(bool get)
{
	double left = prim(get);
	for (;;)
	{
		switch (currToken)
		{
		case MUL: left *= prim(get);
			break;
		case DIV:
			if (double d = prim(get))
			{
				left /= d;
			}
			break;
		default:
			return left;
		}
	}
}

double prim(bool get)
{
	if (get)
	{
		GetToken();
	}
	switch (currToken)
	{
	case NUMBER: {double v = numVal;
		GetToken();
		return v; }
	case NAME: { double& v = table[stringVal];
		if (GetToken() == ASSIGN) v = expr(true);
		return	v; }
	case MINUS: return -prim(true);
		break;
	case LP: { double e = expr(true);
		if (currToken != Token_Value::RP)
		{
			return 0;
		}
		GetToken();
		return e; }
	default:
		return 0;
	}
}

Token_Value GetToken()
{
	char ch = 0;
	cin >> ch;

	switch (ch)
	{
	case 0: return currToken = Token_Value::END;
		break;
	case ';':
	case '*':
	case '/':
	case '+':
	case '-':
	case '(':
	case ')':
	case '=':
		return currToken = Token_Value(ch);

	case '0': case '1': case'2':
	case '3': case '4': case'5':
	case '6': case '7': case'8':
	case '9': case '.':
		cin.putback(ch);
		cin >> numVal;
		return currToken = Token_Value::NUMBER;
	default:
		if (isalpha(ch))
		{
			cin.putback(ch);
			cin >> stringVal;
			return currToken = Token_Value::NAME;
		}
		return currToken = Token_Value::PRINT;
		break;
	}
}